## 简介
CQXQsdk旨在帮助酷Q开发者快速移植插件到先驱上  
你可以直接下载CQXQsdk.zip来使用。

### 简称
下面说明中，CQ代表酷Q，XQ代表先驱。  

### 准备
1. [XQrobot](https://www.xianqubot.com/)   
2. 需要移植的CQ插件源码   
3. 易语言  

### 项目目录描述
├── Readme.md                   // help  
├── sdk                         // （必须下载）  
│&nbsp;&nbsp;├── CQXQsdk.e               // 开发Sdk，类似CQsdk中的：com.example.demo\app.e  
│&nbsp;&nbsp;├── CQXQ模板.ec             // sdk引用模板，类似CQsdk中的：CQPAppSdk.ec   
├── bin                         // （发送语音下载）放进框架bin目录。  
│&nbsp;&nbsp;├── ffmpeg.exe              // 转码器，用作非silk语音的解码。  
│&nbsp;&nbsp;├── silk_v3_encoder.exe     // silk编码器，用作生成silk语音。  
└── Source  
&nbsp;&nbsp;&nbsp;└── CQXQ模板.e              // sdk引用模板源码  
  
### 使用说明  
1. 打开 _CQ插件_ 的源码和 _CQXQsdk.e_  
2. 更换CQ插件的开发模板为： _CQXQ模板.ec_  
3. 将CQ插件的 _AppInfo_ 子程序更换为CQXQsdk.e中的 _AppInfo_ 子程序，并填写插件信息。  
4. 将CQXQsdk.e中的 _XQfunction_ 程序集全部复制到CQ插件中，然后基本移植完成，这时候就可以关闭CQXQsdk.e  
5. 接下来将CQ插件的 __eventGroupMsg_ 子程序中的参数msgId改为文本型。  
6. 将源码编译为 _[你插件的名字].XQ.dll_ ，放进先驱Plugin目录即可。  
  
### 框架目录描述  
├── 先驱.exe  
├── Plugin                      // 插件目录，需要把编译后的xxx.XQ.dll放入  
├── bin                         // 需要发送语音请把ffmpeg.exe 和 silk_v3_encoder.exe 放入。  
└── data                        // 本sdk生成目录，用作存放数据  
&nbsp;&nbsp;&nbsp;├── image                    // 图片缓存目录，用做CQ码发送图片。  
&nbsp;&nbsp;&nbsp;├── record                   // 语音缓存目录，用作CQ码发送语音。  
&nbsp;&nbsp;&nbsp;└── app                      // 插件数据目录，用于记录插件配置信息。  
  
### 调用路径  
XQ框架接收事件→XQfunction(程序集)分发事件→CQ应用(程序集)接收事件→调用CQXQ模板处理事件→CQXQ模板调用XQApi(V1)完成处理  

### 视频教程  
https://www.bilibili.com/video/BV17K4y1e7jV  

### 注意事项  
1. 只能设置一个菜单：menuA  
2. 由于XQ是多QQ框架，而CQ是单Q框架，登陆多Q可能会出现异常。  
3. 本框架只是为了让移植CQ更方便，如果你不是CQ开发者，建议你研究官方Sdk。  
4. 由于XQ框架和CQ框架插件事件和api都不相同，很难做到完美兼容，如果想完美移植插件，建议您重写插件。  
5. 不支持ID点歌  
6. 支持[CQ:xml,content=xxxxxxxxxx] 来发送XML。  
7. 支持[CQ:json,content=xxxxxxxxxx] 来发送Json。  